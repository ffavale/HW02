package unilib;

import unilib.except.*;

public class University
{
    private String name;

    public University(String i_name)
    {
        this.name = i_name;
    }

    public void printUni()
    {

    }

    public void grantDegree(Student i_student, int i_grad_year) throws GraduationException
    {

    }

    public class Course
    {
        private String name;

        public Course(String i_name)
        {
            this.name = i_name;
        }

        public void printCourse()
        {

        }
    }
}
