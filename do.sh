#!/bin/bash

ctags -R *

PROJECT_NAME="TestUniversity"
IS_EXE=1

rm -rf build
mkdir build

if [ $IS_EXE -eq 1 ];
then
echo "Main-Class: $PROJECT_NAME" > build/MANIFEST.txt
else
echo "" >> build/MANIFEST.txt
fi

echo "+++++++++++++ COMPILING +++++++++++++"
find -name "*.java" > build/sources.txt
javac -verbose @build/sources.txt -d build

cd build

find -name "*.class" > bins.txt
jar -cfm $PROJECT_NAME.jar MANIFEST.txt @bins.txt
echo "+++++++++++++++++++++++++++++++++++++"

echo ""
echo ""

cd ..

echo "========= COMPILATION RESULT ========="

jar tf build/$PROJECT_NAME.jar

echo "====================================="

if [ $IS_EXE -eq 1 ];
then
echo ""
echo ""

echo "========== RUNNING PROGRAM =========="

java -jar build/$PROJECT_NAME.jar > build/output.txt
cat build/output.txt

echo "====================================="
fi
